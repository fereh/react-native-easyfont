
const defaultOptions = Object.freeze({
    duration: 3000,
    release: 300,
    rate: 1.0,
    gain: 1.0,
});


/** Manages the playback notes in an instrument. */
class Player {

    /**
     * @param {Instrument} instrument
     * @param {Object} options
     */
    constructor(instrument, options) {
        this.instrument = instrument;
        this.options = defaultOptions;
        this.config(options);
        this.sequence = new Sequence();
        this.activeStreams = [];
    }

    /**
     * Change play options.
     * This is meant to replace passing options to every `play()` call.
     * @param {Object} options - Play options
     */
    config(options) {
        options = { ...this.options, ...options, };

        if (options.speed) {
            options.rate = options.speed;
        }

        // TODO: should these be in Instrument?
        const range = (v, n, x) => ((v < n) && n) || ((v > x) && x) || v;
        options.gain = range(options.gain, 0.0, 1.0);
        options.rate = range(options.rate, 0.5, 2.0);

        this.options = options;
        return this;
    }

    /**
     * Prepare notes for play.
     * Ensures that all notes being played are loaded and ready.
     * @param {String[]} notes - One or more notes
     * @return {Promise<Player>}
     * @example
     *  // Prepare and play a note when ready
     *  player('guitar').prepare('c4').then(p => { p.play('c4') });
     */
    prepare(notes) {
        return this.instrument
            .prepare(notes)
            .then(failedNotes => {
                if (failedNotes.length > 0) {
                    console.debug(failedNotes);
                    throw new Error('Some notes could not be prepared');
                }
                else return this;
            });
    }

    /**
     * Queue notes for playback.
     * @param {String[]|String} notes - One or more notes.
     * @param {Number} [when] - Point on timeline to play notes; millisecond precision.
     * @example
     *  // Play notes at 1 second intervals
     *  player('guitar').prepare('c5', 'e5', 'g5', 'b5').then(player => {
     *      player
     *      .play('c5', 0000)
     *      .play('c5', 1000)
     *      .play('g5', 2000)
     *      .play('b5', 3000);
     *  });
     */
    play(notes, when) {
        // TODO: play silence before/after sounds?
        if (typeof notes === 'string') {
            notes = [ notes, ];
        }
        if (typeof when !== 'number') {
            when = 0;
        }
        return new Promise((resolve, reject) => {
            const playHandler = () => {
                this.instrument
                    .play(notes, this.options.gain, this.options.rate)
                    .then(playCallback)
                    .catch(reject);
            };

            const playCallback = streams => {
                resolve();
                this.activeStreams.push(streams);
                this.sequence.stop(stopHandler, this.options.duration, streams);
            };

            const stopHandler = streams => {
                // TODO: run release envelop before stop
                this.instrument.stop(streams);

                let i = this.activeStreams.findIndex(x => x === streams[0]);
                this.activeStreams.slice(i, i + streams.length - 1);
            };

            this.sequence.start(playHandler, when);
        });
    }

    /**
     * Stop all playing notes.
     */
    stop() {
        this.instrument.stop(this.activeStreams);
        return this;
    }

    /**
     * Pause all playing notes.
     */
    pause() {
        // pauseAll will break other players of same instrument,
        // so pause streams individually
        this.instrument.pause(this.activeStreams);
        return this;
    }

    /**
     * Resume all paused notes.
     */
    resume() {
        this.instrument.resume(this.activeStreams);
        return this;
    }

}

/**
 * @private
 * Controls timing of notes being played via the Player.
 */
class Sequence {
    constructor() {
        this.ts = null;
        this.count = 0;
    }
    progress(ts) {
        if (this.ts === null) {
            this.ts = ts;
            return 0;
        }
        return ts - this.ts;
    }
    start(handler, when, ...args) {
        when -= this.progress(Date.now());
        if (when > 0) {
            setTimeout(handler, when, ...args);
        }
        else handler(...args);
        this.count++;
    }
    stop(handler, when, ...args) {
        setTimeout(() => {
            handler(...args);
            if (--this.count === 0) {
                this.ts = null;
            }
        }, when);
    }
}

module.exports = Player;
