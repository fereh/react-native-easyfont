## Classes

<dl>
<dt><a href="#Player">Player</a></dt>
<dd><p>Manages the playback notes in an instrument.</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#getNotes">getNotes()</a></dt>
<dd><p>List of possible notes for soundfonts.</p>
</dd>
<dt><a href="#getOctaves">getOctaves()</a></dt>
<dd><p>List of possible octaves for soundfonts (always 0-7).</p>
</dd>
<dt><a href="#generatePitchList">generatePitchList()</a></dt>
<dd><p>Generate list of notes in Scientific Pitch Notation, by octave.</p>
</dd>
<dt><a href="#player">player(instrumentName, options)</a> ⇒</dt>
<dd><p>Factory function for <a href="#Player">Player</a>.</p>
</dd>
</dl>

<a name="Player"></a>

## Player
Manages the playback notes in an instrument.

**Kind**: global class  

* [Player](#Player)
    * [new Player()](#new_Player_new)
    * [.config(options)](#Player+config)
    * [.prepare(...notes)](#Player+prepare) ⇒ [<code>Promise.&lt;Player&gt;</code>](#Player)
    * [.play([when], ...notes)](#Player+play)
    * [.stop()](#Player+stop)
    * [.pause()](#Player+pause)
    * [.resume()](#Player+resume)

<a name="new_Player_new"></a>

### new Player()
Call this constructor through the [player](#player) factory function.

<a name="Player+config"></a>

### player.config(options)
Change play options. This is meant to replace passing options to every `play()` call.

**Kind**: instance method of [<code>Player</code>](#Player)  

| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Play options |

<a name="Player+prepare"></a>

### player.prepare(...notes) ⇒ [<code>Promise.&lt;Player&gt;</code>](#Player)
Prepare notes for play. Ensures that all notes being played are loaded and ready.

**Kind**: instance method of [<code>Player</code>](#Player)  

| Param | Type | Description |
| --- | --- | --- |
| ...notes | <code>String</code> | One or more notes |

**Example**  
```javascript
// Prepare and play a note when ready
 player('guitar').prepare('c4').then(p => { p.play('c4') });
```
<a name="Player+play"></a>

### player.play([when], ...notes)
Queue notes for playback.

**Kind**: instance method of [<code>Player</code>](#Player)  

| Param | Type | Description |
| --- | --- | --- |
| [when] | <code>Number</code> | Point on timeline to play notes; millisecond precision. |
| ...notes | <code>String</code> | One or more notes. |

**Example**  
```javascript
// Play notes at 1 second intervals
 player('guitar').prepare('c5', 'e5', 'g5', 'b5').then(player => {
     player
     .play('c5', 0000)
     .play('c5', 1000)
     .play('g5', 2000)
     .play('b5', 3000);
 });
```
<a name="Player+stop"></a>

### player.stop()
Stop all playing notes.

**Kind**: instance method of [<code>Player</code>](#Player)  
<a name="Player+pause"></a>

### player.pause()
Pause all playing notes.

**Kind**: instance method of [<code>Player</code>](#Player)  
<a name="Player+resume"></a>

### player.resume()
Resume all paused notes.

**Kind**: instance method of [<code>Player</code>](#Player)  
<a name="getNotes"></a>

## getNotes()
List of possible notes for soundfonts.

**Kind**: global function  
<a name="getOctaves"></a>

## getOctaves()
List of possible octaves for soundfonts (always 0-7).

**Kind**: global function  
<a name="generatePitchList"></a>

## generatePitchList()
Generate list of notes in Scientific Pitch Notation, by octave.

**Kind**: global function  
<a name="player"></a>

## player(instrumentName, options) ⇒
Factory function for [Player](#Player).

**Kind**: global function  
**Returns**: [Player](#Player)  

| Param | Type |
| --- | --- |
| instrumentName | <code>String</code> | 
| options | <code>Object</code> | 

