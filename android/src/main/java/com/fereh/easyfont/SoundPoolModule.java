package com.fereh.easyfont;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.lang.Object;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Iterator;
import android.util.SparseArray;

import android.media.SoundPool;

public class SoundPoolModule extends ReactContextBaseJavaModule {
    private ReactApplicationContext context;
    private SoundPool realPool;
    private ArrayList<Integer> soundsInPool = new ArrayList<>();
    private HashMap<String, Integer> soundMap = new HashMap<>();
    private SparseArray<ArrayDeque<Callback>> loadCallbacks = new SparseArray<>();
    private Object stateLock = new Object();

    public SoundPoolModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.context = reactContext;
        realPool = null; // waiting for create()
    }

    @Override
    public String getName() {
        return "SoundPool";
    }

    @ReactMethod
    public void create(int maxStreams) {
        // Only one pool can exist at a time for now
        if (realPool != null) {
            release();
        }

        realPool = new SoundPool.Builder()
            .setMaxStreams(maxStreams)
            .build();

        realPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool realPool, int soundId, int status) {
                synchronized (stateLock) {
                    if (status != 0) {
                        soundMap.remove(getKeyFromValue(soundMap, soundId));
                    }
                    else {
                        soundsInPool.add(soundId);
                    }
                }

                // Invoke all pending callbacks
                Callback callback;
                ArrayDeque<Callback> deque = loadCallbacks.get(soundId);
                while (!deque.isEmpty()) {
                    callback = deque.pop();
                    if (status != 0) {
                        callback.invoke("Failed to load sound " + status, null);
                    }
                    else {
                        callback.invoke(null, soundId);
                    }
                }
                loadCallbacks.remove(soundId);
                deque = null;
            }
        });
    }

    @ReactMethod
    public void release() {
        soundMap.clear();
        soundsInPool.clear();
        loadCallbacks.clear();
        realPool.release();
        realPool = null;
    }

    @ReactMethod
    public void load(String name, Callback callback) {
        synchronized (stateLock) {
            if (soundMap.containsKey(name)) {
                if (soundsInPool.contains(soundMap.get(name))) {
                    callback.invoke(null, soundsInPool.get(soundMap.get(name)));
                }
                else {
                    // Sound is still loading, add another callback to queue
                    loadCallbacks.get(soundMap.get(name)).push(callback);
                }
                return;
            }
        }

        int resourceId = context.getResources().getIdentifier(
            name, "raw", context.getPackageName());
        if (resourceId == 0) {
            callback.invoke("Resource not found", null);
            return;
        }
        int soundId = realPool.load(context, resourceId, 1);
        ArrayDeque<Callback> q = new ArrayDeque<>(1);
        q.push(callback);
        loadCallbacks.put(soundId, q);
        soundMap.put(name, soundId);
    }

    @ReactMethod
    public void unload(int soundId) {
        synchronized (stateLock) {
            if (soundsInPool.contains(soundId)) {
                realPool.unload(soundId);
                soundsInPool.remove(soundId);
                soundMap.remove(getKeyFromValue(soundMap, soundId));
            }
        }
    }

    @ReactMethod
    public void play(int soundId, int loops, float rate, float gain, Callback callback) {
        if (!soundsInPool.contains(soundId)) {
            callback.invoke("Sound not loaded", null);
            return;
        }

        int streamId = realPool.play(soundId, gain, gain, 1, loops, rate);
        if (streamId == 0) {
            callback.invoke("Internal error", null);
            return;
        }
        callback.invoke(null, streamId);
    }

    /*@ReactMethod
    public void playSync(ReadableArray soundIds, int loops, float rate, float gain, Callback callback) {
        for (int i = 0; i < soundIds.size(); i++) {
            if (!soundsInPool.contains(soundIds.getInt(i))) {
                callback.invoke("Sound not loaded " + soundIds.getInt(i), -1);
                return;
            }
        }
        WritableArray streamIds = Arguments.createArray();

        for (int i = 0, streamId; i < soundIds.size(); i++) {

            streamId = realPool.play(soundIds.getInt(i), gain, gain, 1, loops, rate);
            if (streamId == 0) {
                callback.invoke("Internal error", -2);
                return;
            }
            streamIds.pushInt(streamId);

        }
        callback.invoke(null, streamIds);
    }*/

    @ReactMethod
    public void stop(int streamId) {
        realPool.stop(streamId);
    }
    @ReactMethod
    public void pause(int streamId) {
        realPool.pause(streamId);
    }
    @ReactMethod
    public void resume(int streamId) {
        realPool.resume(streamId);
    }
    @ReactMethod
    public void autoPause() {
        realPool.autoPause();
    }
    @ReactMethod
    public void autoResume() {
        realPool.autoResume();
    }

    private static String getKeyFromValue(Map hm, int value) {
        for (Object o : hm.keySet()) {
            if (hm.get(o).equals(value)) {
                return o.toString();
            }
        }
        return null;
    }
}
