import React from 'react';
import { AppRegistry, Text, TouchableOpacity, View, } from 'react-native';
import { name as appName } from './app.json';


/*import { NativeModules } from 'react-native';
const { SoundPool } = NativeModules;
SoundPool.create(6)
console.log('SoundPool.load:');
SoundPool.load('acoustic_grand_piano_c4', (e, sound) => {
    if (e) {
        console.log('FAILED');
        console.log(e, sound);
    }
    else {
        console.log(sound);
        console.log('SoundPool.play:');
        SoundPool.play(sound, 0, 1, 1, (e, stream) => {
            if (e) {
                console.log('FAILED');
                console.log(e, stream);
            }
            else {
                console.log(stream);
                console.log('SoundPool.stop:');
                SoundPool.stop(stream);
                SoundPool.release();
                console.log('SoundPool test complete');
            }
        });
    }
});*/


function testSetTimeoutExecutesAtCorrectTime() {
    const timeout = 2000;
    const t1 = performance.now();
    setTimeout(() => {
        const actual = performance.now() - t1;
        if (actual < timeout) {
            console.trace(actual, ' < ', timeout);
            return false;
        }
        console.log(actual);
        return true;
    }, timeout);
}

if (testSetTimeoutExecutesAtCorrectTime() === false) {
    console.log('FAILED: testSetTimeoutExecutesAtCorrectTime');
}

/*import { Instrument, generatePitchList, } from 'react-native-easyfont';
const instrument = new Instrument('acoustic_grand_piano', 6);
instrument.prepare([ 'c4', 'e4', 'g4', ], (e) => {
    if (e) {
        console.log('Prepare chord: FAILED');
        console.log(e);
    }
    instrument.play([ 'c4', ], 1, 1, (streams) => {
        if (!streams || streams.length !== 1) {
            console.log('Play single note: FAILED');
            console.log(streams);
        }
        let _t1 = performance.now();
        setTimeout(() => {
            let _t2 = performance.now();
            if ((_t2 - _t1) < 3000) {
                console.log('setTimeout execution time: FAILED');
                console.log(3000, _t2 - _t1);
                return;
            }
            instrument.play([ 'c4', 'e4', 'g4', ], 1, 1, (streams) => {
                if (!streams || streams.length !== 3) {
                    console.log('Play chord: FAILED');
                    console.log(streams);
                }
            });
        }, 3000);
    });
});*/


/*const piano = player('acoustic_grand_piano', { duration: 800, });
piano.prepare(generatePitchList(2, 3, 4, 5));

function generateProgression() {
    let chords = [
        [ 'c4', 'e4', 'g4' ],
        [ 'c3', 'e3', 'g3' ],
        [ 'c5', 'e5', 'g5' ],
        [ 'd4', 'f4', 'a4' ],
        [ 'd5', 'f5', 'a5' ],
    ];

    let notes = [];
    for (let chord in chords) {
        notes = notes.concat(chord);
    }
    notes = [ ...new Set(notes) ];
    return [ piano.prepare(notes), chords ];
}

let q = [];
q.push(generateProgression());*/

const App = () => {

    const onNext = () => {
        /*let p = q.pop();
        p[0].then(player => {
            for (let i = 0; i < p[1].length; i++) {
                player.play(p[1][i], 1100 * i);
            }
        });
        q.push(generateProgression());*/
    };

    onRepeat = () => {



    };
    return (
        <View>
            <TouchableOpacity onPress={onNext}>
                <Text>Next</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onRepeat}>
                <Text>Repeat</Text>
            </TouchableOpacity>
        </View>
    );
};

AppRegistry.registerComponent(appName, () => App);
