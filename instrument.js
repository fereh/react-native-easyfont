import { NativeModules } from 'react-native';
const { SoundPool } = NativeModules;

const report = function (what, why) {
    return {
        what: what, why: why
    };
};

/** Track progress of multiple async callbacks */
const progress = function (items, step, callback) {
    if (!Array.isArray(items)) {
        console.debug(report(items, 'not array'));
        return;
    }
    notes = [ ...new Set(items), ]; // ensure uniqueness

    let results = [];

    let progress = 0;
    const advance = (result) => {
        if (result !== undefined) {
            results.push(result);
        }
        if (++progress === items.length) {
            callback(results.length !== 0 ? results : null);
        }
    };

    for (let item of items) {
        step(item, advance);
    }
};


const Instrument = function (name, maxStreams) {
    this.name = name;
    this.sounds = {};
    SoundPool.create(maxStreams);
};

Instrument.prototype.release = function () {
    this.sounds = null;
    SoundPool.release();
};

/**
 * Garentee notes are ready for playback.
 * Load any notes that aren't loaded. Notes that fail to be loaded are
 * returned in the callback's argument, allowing for partial success.
 */
Instrument.prototype.prepare = function (notes, callback) {
    progress(notes, (note, done) => {
        note = note.toLowerCase();
        SoundPool.load(`${this.name}_${note}`, (e, sound) => {
            if (e) {
                done(note);
                console.debug(report(note, e));
            }
            else {
                this.sounds[note] = sound;
                done();
            }
        });
    }, callback);
};

Instrument.prototype.play = function (notes, gain, rate, callback) {
    if (typeof gain !== 'number' || typeof rate !== 'number') {
        console.debug(report([ gain, rate], 'not a number'));
        return;
    }

    progress(notes, (note, done) => {
        note = note.toLowerCase();
        let sound = this.sounds[note];
        if (sound === undefined) {
            // this invalid value will be caught in Java
            sound = -1;
        }
        SoundPool.play(sound, 0, gain, rate, (e, stream) => {
            if (e) {
                console.debug(report(note, e));
                return;
            }
            done(stream);
        });
    }, callback);
};

Instrument.prototype.stop = function (streams) {
    for (let stream of streams)
        { SoundPool.stop(stream); }
};

Instrument.prototype.pause = function (streams) {
    for (let stream of streams)
        { SoundPool.pause(stream); }
};

Instrument.prototype.resume = function (streams) {
    for (let stream of streams)
        { SoundPool.resume(stream); }
};

Instrument.prototype.pauseAll = function () {
    SoundPool.autoPause();
};

Instrument.prototype.resumeAll = function () {
    SoundPool.autoResume();
};

Instrument.prototype.setGain = function (streams, gain) {
    // TODO: implement SoundPool.setGain
    //for (let stream of streams)
    //    { SoundPool.setGain(stream, gain); }
};

export default Instrument;
